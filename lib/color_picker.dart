library color_picker;
import 'package:flutter/material.dart';

class ColorPicker extends StatefulWidget {
  ColorPicker({this.colors, this.r, this.g, this.b});

  final List<Color> colors;
  final double r, g, b;

  @override
  _ColorPickerState createState() => _ColorPickerState(colors: colors, r: r, g: g, b: b);
}

class _ColorPickerState extends State<ColorPicker> {
  _ColorPickerState({this.colors, this.r, this.g, this.b});

  List<Color> colors;
  double r, g, b;
  Color _selected = Colors.white;

  @override
  Widget build(BuildContext context) {
    // Show an alertdialog with primary colors and a scroll bar
    return AlertDialog(
      contentPadding: EdgeInsets.all(8.0),
      content: IntrinsicWidth(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            ClipRect(child: 
              Container(
                width: double.infinity,
                height: 180.0,
                child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  children: List<Widget>.generate(colors.length, (int index) {
                    return InkWell(
                      onTap: () {
                        _selected = colors[index];
                      },
                      child: Icon(
                        Icons.brightness_1,
                        color: colors[index],
                      )
                    );
                  }),
                ),
              )
            ),
            Padding(padding: EdgeInsets.fromLTRB(0.0,30.0,0.0,30.0)),
            Container(
              decoration: BoxDecoration(                
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [const Color.fromARGB(255, 0, 0, 0), _selected],
                  tileMode: TileMode.repeated,
                ),
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('CONFIRM'),
          onPressed: () {
            //Close the dialog and pop with the selected color
            Navigator.of(context).pop(_selected);
          },
        ),
      ],
    );
  }
}
