# color_picker

Color picker widget for Flutter, inspired by the new iOS 12 color picker. Uses Material library, but will fit into an iOS project nicely.

## Getting started

`pubspec.yaml`
`
    dependencies:
      color_picker:
        git:
          url: git://gitlab.com/TheNightman/Flutter-Color-Picker
`

